.. SPDX-FileCopyrightText: Huawei Inc.
..
.. SPDX-License-Identifier: CC-BY-4.0

.. include:: ../definitions.rst

Quick start contribution guide for new developers
#################################################

This page describes Quick start contribution guide for new developers who would like to
join the |main_project_name| 


.. contents::
   :depth: 2

Setting up
**********

Creating an account on Eclipse 
------------------------------

Head to the 
`Eclipse foundation website <https://accounts.eclipse.org/user/register?destination=user/login>`_ 
and set up an account by entering your:

- Email
- Username
- Full name
- Organization
- Password
- Country

Then read and check the box to agree to Terms of Use, Privacy Policy and Code of Conduct. 
When you complete that, follow the instructions sent to your email to activate the account.

Signing the ECA
---------------

In order to contribute to the |main_project_name| you need to sign the 
`Eclipse Contributor Agreement  <https://accounts.eclipse.org/user/eca>`_, 
which describes the terms under which you can contribute to the project.

If you sign this ECA, you confirm your legal rights to submit the code to the project.
You also provide license to your contributions to Eclipse and specified users, however
you still own your contributions.

EF Gitlab Account Setup
-----------------------

Now you can go to `the Oniro Gitlab <https://gitlab.eclipse.org/eclipse/oniro-core/oniro>`_ .
You should use the account that was created in the previous step to log in.

For further information, go to the :doc:`Gitlab section <gitlab>`.
