.. SPDX-FileCopyrightText: Huawei Inc.
..
.. SPDX-License-Identifier: CC-BY-4.0

.. include:: definitions.rst

Community Chat Platform
#######################

.. contents::
      :depth: 2

Overview
********

The |main_project_name| project primarily uses a chatroom for community discussions, everyone is welcome!
If you are looking for help or interested in contributing to the project, feel free to interact with maintainers, contributors and other community members.

Project's chatroom is hosted in `Libera IRC Network <https://libera.chat/>`_. which is reachable using `IRC protocol <https://en.wikipedia.org/wiki/Internet_Relay_Chat>`_.
IRC Channel #oniroproject can be joined using any IRC client by connecting to this address:

- `<irc://libera.chat/#oniroproject>`_. 

Alternatively for user's convenience third party services can also be used from web (terms of service apply):

- <https://web.libera.chat/#oniroproject>
- <https://web.libera.chat/gamja/#oniroproject>
- <https://kiwiirc.com/client/irc.libera.chat/#oniroproject>

The user might prefer to install an IRC client to connect to #oniroproject channel, like:

- `HexChat <http://hexchat.github.io/>`_ (Native IRC client for Linux, Windows)

  
More details are explained on `the Libera website <https://libera.chat/guides/connect>`_.

Another possibility to use this IRC room is to use our `Matrix bridge to IRC <https://matrix.to/#/#oniroproject:libera.chat>`_.
One benefit is that everyone can catch up by reading existing logs on reconnection.
